using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIGameplayController : UIBase
{
    [SerializeField] private Transform m_trsContainerTask;
    [SerializeField] private TaskItemController m_taskItem;
    [SerializeField] private TextMeshProUGUI m_levelText;

    private void Awake()
    {
        Debug.Log("UIGameplayController");
    }

    private void OnDestroy()
    {
        UIMenuController.EventStartWithLevel -= StartWithLevel;
        LevelController.EventLevelUpdateTask -= SetUI;
    }

    private void StartWithLevel()
    {
        Show();
    }

    public override void SetupUI()
    {
        Debug.Log("SetupUI");
        UIMenuController.EventStartWithLevel += StartWithLevel;
        LevelController.EventLevelUpdateTask += SetUI;
    }

    public void SetUI(LevelData levelData)
    {
        TransformUtils.DestroyAllChildren(m_trsContainerTask);
        foreach (var item in levelData.taskLevels)
        {
            if (item.Amount == 0)
            {
                continue;
            }
            TaskItemController taskItem = Instantiate(m_taskItem, m_trsContainerTask);
            Color color = GameApp.Instance.BallScriptables[item.ColorType].Color;
            m_levelText.text = "LEVEL: " + levelData.levelID.ToString();
            taskItem.SetUI(color, item.Amount);
        }
    }
}