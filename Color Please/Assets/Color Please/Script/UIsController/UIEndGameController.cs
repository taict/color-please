using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIEndGameController : UIBase
{
    #region DEFINE EVENT

    public delegate void OnEndGame(StageEndGame stageEndGame);

    public static event OnEndGame EventEndGame;

    

    #endregion DEFINE EVENT
    [SerializeField] private Button m_btnPlayAgain;
    [SerializeField] private TextMeshProUGUI m_textTitle;
    [SerializeField] private TextMeshProUGUI m_textButton;

    private void OnDestroy()
    {
        EventEndGame -= OnEventEndGame;
    }

    public override void SetupUI()
    {
        Debug.Log("SetupUI");
        EventEndGame += OnEventEndGame;
    }

    private void OnEventEndGame(StageEndGame stageEndGame)
    {
        Debug.Log("OnEventEndGame");
        Show();
        switch (stageEndGame)
        {
            case StageEndGame.LOSE:
                {
                    m_btnPlayAgain.onClick.AddListener(OnPlayAgain);
                    m_textTitle.text = "GAME OVER";
                    m_textButton.text = "PLAY AGAIN";
                }
                break;
            case StageEndGame.WIN:
                {
                    m_btnPlayAgain.onClick.AddListener(OnPlayNext);
                    m_textTitle.text = "GAME WIN";
                    m_textButton.text = "NEXT GAME";
                }
                break;
        }
    }

    private void OnPlayAgain()
    {
        GameManager.Instance.LoadReplayLevel();
        Hide();
    }

    private void OnPlayNext()
    {
        GameManager.Instance.LoadNextLevel();
        //GameManager.Instance.LoadReplayLevel();
        Hide();
    }

    public void OnTriggerEventEndGame(StageEndGame stageEndGame)
    {
        EventEndGame(stageEndGame);
    }
}