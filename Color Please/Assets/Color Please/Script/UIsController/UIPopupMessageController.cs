using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupMessageController : UIBase
{
    [SerializeField] private TextMeshProUGUI m_tmpMessage;
    [SerializeField] private Button m_btnOk;
    [SerializeField] private Button m_btnClose;

    private void Awake()
    {
        m_btnClose.onClick.AddListener(Hide);
        m_btnOk.onClick.AddListener(Hide);
    }

    public void Show(string ms)
    {
        base.Show();
        m_tmpMessage.text = ms;
    }
}