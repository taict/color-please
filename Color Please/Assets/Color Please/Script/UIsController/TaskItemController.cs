using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TaskItemController : MonoBehaviour
{
    [SerializeField] private Image m_img;
    [SerializeField] private TextMeshProUGUI m_tmpAmount;

    public void SetUI(Color color, int amount)
    {
        m_img.color = color;
        m_tmpAmount.text = amount.ToString();
    }

    public void SetAmout(int amount)
    {
        m_tmpAmount.text = amount.ToString();
    }
}