using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenuController : UIBase
{
    #region DEFINE EVENT

    public delegate void OnStartWithLevel();

    public static event OnStartWithLevel EventStartWithLevel;

    #endregion DEFINE EVENT

    [SerializeField] private Button m_btnPlay;

    private void Awake()
    {
        m_btnPlay.onClick.AddListener(OnPlay);
        EventStartWithLevel += StartLevel;
    }

    private void OnDestroy()
    {
        EventStartWithLevel -= StartLevel;
    }

    private void StartLevel()
    {
        Hide();
    }

    private void OnPlay()
    {
        EventStartWithLevel?.Invoke();
    }
}