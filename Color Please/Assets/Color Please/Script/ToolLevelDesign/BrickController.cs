using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickController : MonoBehaviour
{
    private BallScriptableObject m_colorConfig;
    private LevelDesignManager m_levelDesign;
    private int m_indexColor = 0;
    private Renderer m_render;
    private int m_numberColor;
    private ColorPoint m_colorPoint;
    private bool m_isPress = false; 

    public BallScriptableObject ColorConfig { get => m_colorConfig; }
    public ColorPoint ColorPoint { get => m_colorPoint; }
    public bool IsPress { get => m_isPress; }

    private void Awake()
    {
        m_render = GetComponent<Renderer>();
    }

    private void OnMouseDown()
    {
        m_indexColor++;
        if (m_indexColor >= m_numberColor)
        {
            m_indexColor = 0;
        }

        OnSetColor();
        m_levelDesign.ModifiCurrentLevelData(this);
    }

    public void Setup(LevelDesignManager levelDesignManager, int indexX, int indexY)
    {
        this.m_levelDesign = levelDesignManager;
        this.m_indexColor = 0;
        this.m_numberColor = m_levelDesign.ColorsConfig.Count;

        m_colorPoint = new ColorPoint();
        m_colorPoint.indexX = indexX;
        m_colorPoint.indexY = indexY;

        OnSetColor();
    }
    public void RemoveBrick()
    {
        m_isPress = false;
        m_indexColor = 0;
        OnSetColor();
        m_levelDesign.ModifiCurrentLevelData(this);
    }
    public void OnFillColor(BallScriptableObject colorConfig)
    {
        m_colorConfig = colorConfig;

        m_colorPoint.colorCode = (int)m_colorConfig.ColorType;
        m_render.material.color = ColorConfig.Color;
    }

    private void OnSetColor()
    {
        m_colorConfig = m_levelDesign.ColorsConfig[m_indexColor];

        m_colorPoint.colorCode = (int)m_colorConfig.ColorType;
        m_render.material.color = ColorConfig.Color;
    }
}