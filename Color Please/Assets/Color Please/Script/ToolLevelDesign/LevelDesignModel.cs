using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelContainer
{
    public List<LevelData> levelDatas;

    public LevelContainer()
    {
        ColorPoint colorPoint = new ColorPoint()
        {
            indexX = 0,
            indexY = 0,
            colorCode = 2
        };
        LevelData levelData = new LevelData()
        {
            levelID = 1,
            sizeX = 5,
            sizeY = 10,
            spacePer = 1,
            colorPoints = new List<ColorPoint> { colorPoint }
        };

        levelDatas = new List<LevelData>() { levelData };
    }
}

[System.Serializable]
public class LevelData
{
    public int levelID;
    public int sizeX;
    public int sizeY;
    public float spacePer;

    public List<TaskLevel> taskLevels;
    public List<ColorPoint> colorPoints;
}

[System.Serializable]
public class ColorPoint
{
    public int indexX;
    public int indexY;
    public int colorCode;
}