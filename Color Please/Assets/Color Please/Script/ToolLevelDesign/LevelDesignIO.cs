using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LevelDesignIO : MonoBehaviour
{
    public string TempFilePath { get; private set; }
    public string ReadFilePath { get; private set; }
    public LevelContainer levelContainer;

    public void Init(Action OnComplete)
    {
        Debug.Log("Init");
        //TempFilePath = Application.dataPath + "/Resources/Levels/levels.txt";
        TempFilePath = Application.persistentDataPath + "/levels.data";
        ReadFilePath = "Levels/levels.txt";
        Debug.LogError("TempFilePath ==> " + TempFilePath);
#if UNITY_EDITOR
        if (!File.Exists(TempFilePath))//make a file if not exists
        {
            using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
            {
                Debug.Log("CREATE NEW LEVEL CONTAINER");
                levelContainer = new LevelContainer();
                newTask.WriteLine(JsonUtility.ToJson(levelContainer));
                newTask.Close();
            }
        }
        else// read and update file
        {
            StreamReader reader = new StreamReader(TempFilePath);
            var read = Resources.Load<TextAsset>("Levels/levels");
            JsonUtility.FromJsonOverwrite(reader.ReadLine(), levelContainer);
            reader.Close();
        }
#else

        var read = Resources.Load<TextAsset>("Levels/levels");
        JsonUtility.FromJsonOverwrite(read.ToString(), levelContainer);

#endif
        OnComplete?.Invoke();
    }

    public void SaveData(LevelData levelData)
    {
        Debug.Log("SAVE DATA");
        if (TempFilePath != null)
        {
            using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
            {
                var lv = levelContainer.levelDatas.Find(lv => lv.levelID == levelData.levelID);
                if (lv == null)
                {
                    Debug.Log("Save new level");
                    levelContainer.levelDatas.Add(levelData);
                }
                else
                {
                    Debug.Log("replace level");
                    lv = levelData;
                }
                string str = JsonUtility.ToJson(levelContainer);
                newTask.WriteLine(str);
                newTask.Close();
            }
        }
    }
}