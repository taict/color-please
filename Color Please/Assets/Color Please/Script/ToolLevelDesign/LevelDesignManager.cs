using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelDesignManager : MonoBehaviour
{
    const int SPACE_DEFAULT = 1;
    [SerializeField] private int m_currentLevelID = 1;

    [Range(3, 5)]
    [SerializeField] private int sizeX = 4;

    [Range(5, 10)]
    [SerializeField] private int sizeY = 10;

    [SerializeField] private float m_spacePer = 1;
    [SerializeField] private List<TaskLevel> m_taskLevels;
    [SerializeField] private GameObject m_brickPrefab;
    [SerializeField] private List<BallScriptableObject> colorsConfig;
    [SerializeField] private TextMeshProUGUI m_tmpLevel;
    private GameObject[,] m_nodes;
    private LevelDesignIO m_levelDesignIO;
    private LevelData m_currentLevelData;
    public List<BallScriptableObject> ColorsConfig { get => colorsConfig; }

    private void Awake()
    {
        m_levelDesignIO = GetComponent<LevelDesignIO>();
        colorsConfig = colorsConfig.OrderBy(item => (int)item.ColorType).ToList();
    }

    private void Start()
    {
        m_levelDesignIO.Init(() => { LoadLevel(m_currentLevelID); });
    }

    private void OnApplicationQuit()
    {
        OnSave();
    }

    public void ModifiCurrentLevelData(BrickController brick)
    {
        int newColorCode = brick.ColorPoint.colorCode;
        var colorPoint = m_currentLevelData.colorPoints.Find(clPoint => clPoint.indexX == brick.ColorPoint.indexX
                                                           && clPoint.indexY == brick.ColorPoint.indexY);
        if (newColorCode == (int)ColorType.NONE)
        {
            m_currentLevelData.colorPoints.Remove(colorPoint);
            return;
        }

        if (colorPoint != null)
        {
            colorPoint.colorCode = newColorCode;
            return;
        }
        m_currentLevelData.colorPoints.Add(brick.ColorPoint);
    }

    private void LoadLevel(int lvID)
    {
        m_currentLevelData = m_levelDesignIO.levelContainer.levelDatas.Find(lv => lv.levelID == lvID);
        m_taskLevels = m_currentLevelData.taskLevels;
        if (m_currentLevelData == null)
        {
            if (m_levelDesignIO.levelContainer.levelDatas.Count > 0)
            {
                m_levelDesignIO.levelContainer.levelDatas.RemoveAt(0);
            }
            SetValueLevelText($"Level {SPACE_DEFAULT}");     
            m_spacePer = SPACE_DEFAULT;
            BuildLevelWithSize(this.sizeX, this.sizeY);
            return;
        }
        
        m_tmpLevel.text = $"Level {lvID}";
        m_spacePer = m_currentLevelData.spacePer;
        BuildLevelWithSize(m_currentLevelData.sizeX, m_currentLevelData.sizeY);
        FillDataIntoBoard(lvID);
    }

    private void BuildLevelWithSize(int x, int y)
    {
        this.sizeX = x;
        this.sizeY = y;
        BuildLevelEmpty();
    }

    private void FillDataIntoBoard(int levelID)
    {
        var lsColorPoint = m_currentLevelData.colorPoints;
        foreach (var colorPoint in lsColorPoint)
        {
            BallScriptableObject colorConfig = ColorsConfig.Find(color => (int)color.ColorType == colorPoint.colorCode);
            if (colorConfig == null)
            {
                Debug.LogError($"Color code error: {colorPoint.colorCode}");
                continue;
            }
            m_nodes[colorPoint.indexX, colorPoint.indexY].GetComponent<BrickController>().OnFillColor(colorConfig);
        }
    }

    private void BuildLevelEmpty()
    {
        m_nodes = new GameObject[sizeX, sizeY];
        Vector3 newPos = Vector3.zero;
        for (int y = 0; y < this.sizeY; y++)
        {
            for (int x = 0; x < this.sizeX; x++)
            {
                GameObject point = Instantiate(this.m_brickPrefab);
                //GameObject point = GameObject.CreatePrimitive(PrimitiveType.Cube);
                point.name = $"point {y}-{x}";
                point.transform.parent = this.transform;
                point.transform.localPosition = newPos;
                newPos.x += this.m_spacePer;
                m_nodes[x, y] = point;
                m_nodes[x, y].gameObject.GetComponent<BrickController>().Setup(this, x, y);
            }
            newPos.z -= this.m_spacePer;
            newPos.x = 0;
        }

        Vector3 cameraPos = Camera.main.transform.localPosition;
        cameraPos.x = m_nodes[this.sizeX / 2, this.sizeY / 2].transform.position.x;
        cameraPos.z = -this.sizeY * this.m_spacePer / 2.0f;
        Camera.main.transform.localPosition = cameraPos;
    }

    private void ClearLevel()
    {
        TransformUtils.DestroyAllChildren(transform);
        m_nodes = null;
        m_taskLevels = null;
        m_currentLevelData = null;
    }

    private void OnSave()
    {
        if (m_currentLevelData != null)
        {
            m_currentLevelData.taskLevels = m_taskLevels;
            m_currentLevelData.spacePer = m_spacePer;
            m_currentLevelData.sizeX = this.sizeX;
            m_currentLevelData.sizeY = this.sizeY;
        }
        m_levelDesignIO.SaveData(m_currentLevelData);
    }
    private void SetValueLevelText(string str)
    {
        m_tmpLevel.text = str;
    }

    #region EVENT BUTTON

    public void OnSaveButton()
    {
        Debug.Log("OnSaveButton");
        OnSave();
    }

    public void OnCreateButton()
    {
        Debug.Log("OnCreateButton");
        OnSave();
        ClearLevel();
        BuildLevelWithSize(this.sizeX, this.sizeY);
        this.m_currentLevelID = m_levelDesignIO.levelContainer.levelDatas.Count + 1;
        m_currentLevelData = new LevelData()
        {
            levelID = this.m_currentLevelID,
            sizeX = this.sizeX,
            sizeY = this.sizeY,
            spacePer = this.m_spacePer,
            colorPoints = new List<ColorPoint>()
        };
        SetValueLevelText($"Level {m_currentLevelID}");
        OnSave();
    }

    public void OnNextButton()
    {
        Debug.Log("OnNextButton");
        m_currentLevelID++;
        if (m_currentLevelID > m_levelDesignIO.levelContainer.levelDatas.Count)
        {
            m_currentLevelID--;
            return;
        }
//        OnSave();
        ClearLevel();
        LoadLevel(m_currentLevelID);
    }

    public void OnBackButton()
    {
        Debug.Log("OnBackButton");
        m_currentLevelID--;
        if (m_currentLevelID < 1)
        {
            m_currentLevelID++;
            return;
        }
 //       OnSave();
        ClearLevel();
        LoadLevel(m_currentLevelID);
    }

    public void OnDeleteButton()
    {
        Debug.Log("OnDeleteButton");
        if (this.m_levelDesignIO.levelContainer.levelDatas.Count < 1)
        {
            SetValueLevelText("DON'T DELETE LEVEL");
            return;
        }
        this.m_levelDesignIO.levelContainer.levelDatas.Remove(m_levelDesignIO.levelContainer.levelDatas.Find(item => item.levelID == m_currentLevelID));
        int coutLevel = this.m_levelDesignIO.levelContainer.levelDatas.Count;
        var levelDatas = this.m_levelDesignIO.levelContainer.levelDatas;
        for (int index = 0; index < coutLevel; index++)
        {
            levelDatas[index].levelID = index + 1;
        }

        OnBackButton();
    }

    #endregion EVENT BUTTON
}