using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ball")]
public class BallScriptableObject : ScriptableObject
{
    public ColorType ColorType;
    public ColorGroup ColorGroup = ColorGroup.PRIMARY;
    public List<ColorType> ColorsRequestMix;
    public string Name;
    public Color Color;
}