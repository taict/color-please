using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridController : MonoBehaviour
{
    public Transform GridCellPrefab;
    public NodeController[,] nodes;
    public Plane Plane;
    public float distanPerPoint;

    private bool m_isShowed = false;
    private float m_horizontalDistance = 0;
    private float m_verticalDistance = 0;
    private int mid = 0;

    public int height;
    public int width;
    [SerializeField] private Transform parent0;
    [SerializeField] private Transform parent1;
    [SerializeField] private Transform parent2;
    [SerializeField] private List<CubeController> cubeControllers = new List<CubeController>();
    [SerializeField] private NodeController m_nodeControllerPrefab;
    [SerializeField] private CubeController m_brick;
    private LevelData m_levelData;
    private PlayerController m_player;

   
    public void Init(PlayerController player, LevelData levelData)
    {
        m_player = player;

        m_levelData = new LevelData()
        {
            taskLevels = new List<TaskLevel>(),
            colorPoints = new List<ColorPoint>(),
            levelID = levelData.levelID,
            sizeX = levelData.sizeX,
            sizeY = levelData.sizeY,
            spacePer = levelData.spacePer
        };

        m_levelData.taskLevels.AddRange(levelData.taskLevels.ToArray());
        m_levelData.colorPoints.AddRange(levelData.colorPoints.ToArray());
        width = m_levelData.sizeX;
        height = m_levelData.sizeY;
        Plane = new Plane(Vector3.up, transform.position);
        GridCellPrefab.gameObject.SetActive(true);
        m_player.cubes.Clear();
        BuildLevel();
    }

    private void BuildLevel()
    {
        nodes = new NodeController[m_levelData.sizeX, m_levelData.sizeY];
        Vector3 newPos = Vector3.zero;
        for (int y = 0; y < m_levelData.sizeY; y++)
        {
            for (int x = 0; x < m_levelData.sizeX; x++)
            {
                NodeController point = Instantiate(m_nodeControllerPrefab);
                point.name = $"point {y}-{x}";
                point.transform.parent = parent0.transform;
                point.transform.localPosition = newPos;

                GameObject floor = GameObject.CreatePrimitive(PrimitiveType.Cube);
                floor.GetComponent<Renderer>().material.color = Color.grey;
                floor.name = $"floor {y}-{x}";
                floor.transform.parent = parent1.transform;
                floor.transform.localPosition = newPos + Vector3.down / 2;

                newPos.x += m_levelData.spacePer;
                nodes[x, y] = point;
            }
            newPos.z -= m_levelData.spacePer;
            newPos.x = 0;
        }
        foreach (var item in m_levelData.colorPoints)
        {
            CubeController cube = Instantiate(m_brick, nodes[item.indexX, item.indexY].transform);
            m_player.cubes.Add(cube);
            cube.transform.localPosition = Vector3.zero;
            cube.Setup(this, m_player, item.indexX, item.indexY);
        }

        Vector3 cameraPos = Camera.main.transform.localPosition;
        cameraPos.x = nodes[m_levelData.sizeX / 2, m_levelData.sizeY / 2].transform.position.x;
        cameraPos.z = -m_levelData.sizeY * m_levelData.spacePer / 2.0f;
        Camera.main.transform.localPosition = cameraPos;
    }

    public void Show()
    {
        if (m_isShowed)
        {
            return;
        }
        m_isShowed = true;
        gameObject.SetActive(true);
    }

    [ContextMenu("CreateGrid Auto")]
    public void CreateGridAuto()
    {
        nodes = new NodeController[width, height];
        mid = width / 2;
        var name = 0;
        m_horizontalDistance = 0;
        m_verticalDistance = 0;
        Debug.Log($"k {m_horizontalDistance}");
        for (int i = mid; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Vector3 worldPosition = new Vector3(m_horizontalDistance, 0, m_verticalDistance);
                if (i == 1) { nodes[i, j] = Instantiate(m_nodeControllerPrefab, parent1); } else { if (i == 2) { nodes[i, j] = Instantiate(m_nodeControllerPrefab, parent2); } }
                nodes[i, j].transform.localPosition = worldPosition;
                nodes[i, j].SetUpNodeController(true, worldPosition, m_nodeControllerPrefab, null);
                nodes[i, j].name = "point " + name;
                name++;
                m_verticalDistance -= distanPerPoint;
            }
            m_verticalDistance = 0;

            m_horizontalDistance += distanPerPoint;
            Debug.Log($"k {m_horizontalDistance}");
        }
        m_horizontalDistance = 0f;
        Debug.Log($"k {m_horizontalDistance}");
        for (int i = 0; i < mid; i++)
        {
            m_horizontalDistance -= distanPerPoint;
            Debug.Log($"k {m_horizontalDistance}");
            for (int j = 0; j < height; j++)
            {
                Vector3 worldPosition = new Vector3(m_horizontalDistance, 0, m_verticalDistance);
                nodes[i, j] = Instantiate(m_nodeControllerPrefab, parent0);
                nodes[i, j].transform.localPosition = worldPosition;
                nodes[i, j].SetUpNodeController(true, worldPosition, m_nodeControllerPrefab, null);
                nodes[i, j].name = "point " + name;
                name++;
                m_verticalDistance -= distanPerPoint;
            }
            m_verticalDistance = 0;
        }
    }

    public NodeController GetNode(int indexI, int indexJ)
    {
        if (this.nodes == null)
        {
            Debug.Log("Calculator " + null);
            Debug.Log("Calculator " + nodes == null);
            return null;
        }
        if (indexI >= width || indexJ > height || indexI < 0 || indexJ < 0)
        {
            Debug.Log("GetNode " + indexI);
            Debug.Log("GetNode " + indexJ);
            return null;
        }
        Debug.Log("index I: " + indexI);
        Debug.Log("index J: " + indexJ);
        return this.nodes[indexI, indexJ];
    }
}