using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    #region DEFINE EVENT

    public delegate void OnLevelUpdateTask(LevelData levelData);

    public static OnLevelUpdateTask EventLevelUpdateTask;

    #endregion DEFINE EVENT

    [SerializeField] private StageLevel m_stageLevel;
    [SerializeField] private GridController m_Grid;
    [SerializeField] private PlayerController m_playerController;
    [SerializeField] private LevelData m_levelData;
    public StageLevel StageLevel { get => m_stageLevel; set => m_stageLevel = value; }
    public PlayerController Player { get => m_playerController; set => m_playerController = value; }
    public GridController Grid { get => m_Grid; set => m_Grid = value; }
    public LevelData LevelData { get => m_levelData; set => m_levelData = value; }

    private void Awake()
    {
        UIEndGameController.EventEndGame += OnEventEndGame;
    }
    private void OnDestroy()
    {
        UIEndGameController.EventEndGame -= OnEventEndGame;
    }

    private void OnEventEndGame(StageEndGame stageEndGame)
    {
        m_stageLevel = StageLevel.End;
    }

    public void InitLevel(LevelData levelData)
    {
        m_levelData = new LevelData()
        {
            taskLevels = new List<TaskLevel>(),
            colorPoints = new List<ColorPoint>(),
            levelID = levelData.levelID,
            sizeX = levelData.sizeX,
            sizeY = levelData.sizeY,
            spacePer = levelData.spacePer
        };
        m_levelData.taskLevels.AddRange(levelData.taskLevels.ToArray());
        m_levelData.colorPoints.AddRange(levelData.colorPoints.ToArray());

        StageLevel = StageLevel.Playing;
        m_playerController.Init(this);
        m_Grid.Init(m_playerController, m_levelData);
        LevelUpdateTask();
    }

    public void LevelUpdateTask()
    {
        EventLevelUpdateTask(m_levelData);
    }
}