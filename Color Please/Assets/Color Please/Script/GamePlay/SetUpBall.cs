using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SetUpBall : MonoBehaviour
{
    public BallScriptableObject Ball;
    public Color Color;
    //[SerializeField] TextMeshProUGUI m_text;
    [SerializeField] string m_name;
    [SerializeField] Renderer m_renderer;
    public int m_value;

    public void SetUpBallColor()
    {   
        this.m_name = Ball.Name;
        this.Color = Ball.Color;   
        //this.m_text.text = $"{(int)Ball.ColorType}";
        //this.m_value = (int)Ball.ColorType;       
    }
    public void UpdateValue(BallScriptableObject item)
    {
        Ball = item;        
        SetUpBallColor();
    }
}
