using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private const string CUBE = "Cube";
    [SerializeField] private bool IsOnGrid;
    [SerializeField] private Vector3 ObjStartPosition;
    [SerializeField] private Vector3 SmoothMousePosition;
    [SerializeField] private LevelController m_level;
    [SerializeField] private CubeController target;
    [SerializeField] private GridController m_grid;
    [SerializeField] private CubeController m_cubeSpawn;
    
    public List<CubeController> cubes = new List<CubeController>();
    public List<GameObject> ld;

    private int m_count;
    private int sum;   

    public int m_countDestroyCubeSpecial = 0;
    public bool IsSetScore = false;

    public CubeController GetTarget
    { get { return target; } }

    public LevelController Level { get => m_level; }

    private Renderer Renderer;
    public int m_indexNodeI;
    public int m_indexNodeJ;

    private Vector3 m_mousePosition;
    List<TaskLevel> m_tasks;
    public void SetTarget(CubeController target)
    {
        this.target = target;
    }

    public void Init(LevelController level)
    {
        m_level = level;
    }

    public void DragMultipleBall()
    {
        Debug.Log("target.m_indexNodeI " + target.m_indexNodeI);
        Debug.Log("m_indexNodeJ " + target.m_indexNodeJ);
        Vector3 newPos = Vector3.zero;
        for (int i = target.m_indexNodeJ + 1; i < m_grid.height; i++)
        {

            if (m_grid.nodes[target.m_indexNodeI, i].IsChild() == false)
            {
                return;
            }
            Transform cube = m_grid.nodes[target.m_indexNodeI, i].transform.GetChild(0).gameObject.transform;
            cube.transform.parent = target.cubeContainer;
            newPos.z -= m_level.LevelData.spacePer;
            cube.transform.SetAsLastSibling();
            cube.transform.localPosition = newPos;

        }
    }
    public void CheckOutSide()
    {
        Debug.Log("!!!!!CheckOutSide*******");
        foreach (CubeController item in cubes)
        {
            if (item.ObjStartPosition.z <= -m_grid.height)
            {
                UIManager.Instance.UIEndGame.OnTriggerEventEndGame(StageEndGame.LOSE);
                Debug.Log("=============================================");
                return;
            }
        }
    }
    public IEnumerator DropMultipleCube(int indexI, int indexJ)
    {
        Debug.Log("m_countDestroyCubeSpecial " + m_countDestroyCubeSpecial);
        if (indexJ - 1 < 0 || m_grid.GetNode(indexI, indexJ).transform.childCount == 0)
        {           
            Debug.Log("=======================> Calculator");
            Debug.Log("m_player.IsSetScore " + (IsSetScore));
            if (IsSetScore == false)
            {
                InstantiateCube();
                CheckDeathZone();
                IsSetScore = false;
            }
            yield break;
        }            
        Debug.Log("DropMultipleCube");
        CubeController cubeTarget = m_grid.GetNode(indexI, indexJ).transform.GetChild(0).GetComponent<CubeController>();
        CubeController cubeObj = m_grid.GetNode(indexI, indexJ - 1).transform.GetChild(0).GetComponent<CubeController>();
        BallScriptableObject check = GameApp.Instance.MixColors(cubeTarget.m_setUpBall.Ball.ColorType, cubeObj.m_setUpBall.Ball.ColorType);
        Debug.Log("check " + check);
        Debug.Log(cubeTarget.m_setUpBall.m_value);
        Debug.Log(cubeObj.m_setUpBall.m_value);
        Debug.Log("cubeTarget.m_setUpBall.Ball.ColorType == ColorType.BLACK " + (cubeTarget.m_setUpBall.Ball.ColorType == ColorType.BLACK));
        if (cubeTarget.m_setUpBall.Ball.ColorType == ColorType.BLACK)
        {
            Destroy(cubeTarget.gameObject, Constant.DESTROYBALLTIME);          
            Destroy(cubeObj.gameObject, Constant.DESTROYBALLTIME);
            cubes.Remove(cubeTarget);
            cubes.Remove(cubeObj);
            for (int i = 0; i < ld.Count; i++)
            {
                Debug.Log("chay UpMultipleBall Destroy");
                Debug.Log($"ld{i} " + ld[i].name);
                Debug.Log(target.m_indexNodeI + " " + (target.m_indexNodeJ + i) + " " + i);
                CubeController cube = ld[i].transform.GetComponent<CubeController>();
                if (target != null)
                {
                    Debug.Log("target != null " + target != null);
                    Debug.Log(cubeObj.ObjStartPosition - new Vector3(0, 0, (i)));
                    Debug.Log(cube.GetComponent<CubeController>().m_setUpBall.m_value);
                    //cubeTarget = ld[0].transform.GetComponent<CubeController>();
                    cube.transform.parent = m_grid.nodes[cubeObj.m_indexNodeI, (cubeObj.m_indexNodeJ + i)].transform;
                    cube.transform.DOMove(cubeObj.ObjStartPosition - new Vector3(0, 0, (i)), Constant.DURATIONTIME).SetId(cube).OnComplete(() =>
                    {
                        //cube.transform.localPosition = new Vector3(0, 0, 0);
                    });
                    cube.ObjStartPosition = cubeObj.ObjStartPosition - new Vector3(0, 0, (i));
                    cube.m_indexNodeI = cubeObj.m_indexNodeI;
                    cube.m_indexNodeJ = cubeObj.m_indexNodeJ + i;
                }
            }
            yield break;
        }
        if (check != null)
        {
            Debug.Log("chay ");
            IsSetScore = true;            
            Debug.Log("cubeTarget.m_setUpBall.Ball.ColorType " + cubeTarget.m_setUpBall.Ball.ColorType);
            Debug.Log("cubeObj.m_setUpBall.Ball.ColorType " + cubeObj.m_setUpBall.Ball.ColorType);
            BallScriptableObject m_colorType = GameApp.Instance.MixColors(cubeTarget.m_setUpBall.Ball.ColorType, cubeObj.m_setUpBall.Ball.ColorType);
            Debug.Log("GameApp.Instance.BallScriptables[m_colorType.ColorType] " + m_colorType.ColorType);
            cubeTarget.m_setUpBall.UpdateValue(GameApp.Instance.BallScriptables[m_colorType.ColorType]);
            cubeTarget.SetColor();
            cubeTarget.ObjStartPosition = cubeObj.ObjStartPosition;
            cubeTarget.transform.parent = cubeObj.transform.parent;
            cubeTarget.transform.DOMove(cubeObj.ObjStartPosition, Constant.DURATIONTIME).SetId(this).OnComplete(() =>
            {
                //cubeTarget.transform.localPosition = new Vector3(0,0,0);
            });
            yield return new WaitForSeconds(Constant.AFTERMOVETIME);
            cubeTarget.m_indexNodeI = cubeObj.m_indexNodeI;
            cubeTarget.m_indexNodeJ = cubeObj.m_indexNodeJ;
            int index = cubes.FindIndex(item => item == cubeObj);
            if (index >= 0)
            {
                cubes.RemoveAt(index);
            }
            DestroyImmediate(cubeObj.gameObject);
            Vector3 newPos = new Vector3(0, 0, Constant.BALLDESTROYPOSZ);
            var task = m_level.LevelData.taskLevels;
            foreach (var item in task)
            {
                if (cubeTarget.m_setUpBall.Ball.ColorType == item.ColorType && item.Amount > 0)
                {
                    Debug.Log("item.Amount >= 0  " + (item.Amount));
                    item.Amount--;
                    m_level.LevelUpdateTask();
                    Debug.Log("item.Amount >= 0  " + (item.Amount));
                    cubeTarget.transform.DOMove(newPos, Constant.DURATIONTIME).SetId(cubeTarget);
                    cubeTarget.transform.parent = null;
                    cubeTarget.IsDestroy = true;
                    Destroy(cubeTarget.gameObject, Constant.DESTROYBALLTIME);
                    cubes.Remove(cubeTarget);
                    Debug.Log("cubeTargetDestroy " + " ======== " + (cubeTarget.IsDestroy == true));
                    m_countDestroyCubeSpecial++;
                }
            }            
        }
        if (cubeTarget.IsDestroy == true)
        {
            Debug.Log("cubeTarget == null ");
            Debug.Log("cubeTargetDestroy " + cubeTarget.name);
            for (int i = 0; i < ld.Count; i++)
            {
                Debug.Log("chay UpMultipleBall Destroy");
                Debug.Log($"ld{i} " + ld[i].name);
                Debug.Log(target.m_indexNodeI + " " + (target.m_indexNodeJ + i) + " " + i);
                CubeController cube = ld[i].transform.GetComponent<CubeController>();                
                if (target != null)
                {
                    Debug.Log("target != null " + target != null);
                    Debug.Log(target.ObjStartPosition - new Vector3(0, 0, (i)));
                    Debug.Log(cube.GetComponent<CubeController>().m_setUpBall.m_value);
                    //cubeTarget = ld[0].transform.GetComponent<CubeController>();
                    if ((target.m_indexNodeJ + i + 1) >= m_grid.height)
                    {
                        Debug.Log("Out Array");
                        UIManager.Instance.UIEndGame.OnTriggerEventEndGame(StageEndGame.LOSE);
                        yield break;
                    }
                    if ((target.m_indexNodeJ + i + 1) >= m_grid.height)
                    {
                        Debug.Log("Out Array");
                        UIManager.Instance.UIEndGame.OnTriggerEventEndGame(StageEndGame.LOSE);
                        yield break;
                    }
                    cube.transform.parent = m_grid.nodes[target.m_indexNodeI, (target.m_indexNodeJ + i)].transform;
                    cube.transform.DOMove(target.ObjStartPosition - new Vector3(0, 0, (i)), Constant.DURATIONTIME).SetId(cube).OnComplete(() =>
                    {
                        //cube.transform.localPosition = new Vector3(0, 0, 0);
                    });
                    cube.ObjStartPosition = target.ObjStartPosition - new Vector3(0, 0, (i));
                    cube.m_indexNodeI = target.m_indexNodeI;
                    cube.m_indexNodeJ = target.m_indexNodeJ + i;
                }
            }            
        }
        if(cubeTarget.IsDestroy == false)
        {
            for (int i = 0; i < ld.Count; i++)
            {
                Debug.Log("chay UpMultipleBall");
                Debug.Log("ld[0] " + (ld[0].transform.GetComponent<CubeController>().m_setUpBall.m_value == cubeTarget.m_setUpBall.m_value));
                CubeController cube = ld[i].transform.GetComponent<CubeController>();
                Debug.Log(target.m_indexNodeI + " target.m_indexNode " + target.m_indexNodeJ);
                
                if (target != null)
                {
                    Debug.Log("cube i " + i);
                    if ((target.m_indexNodeJ + i + 1) >= m_grid.height)
                    {
                        Debug.Log("Out Array");
                        UIManager.Instance.UIEndGame.OnTriggerEventEndGame(StageEndGame.LOSE);
                        yield break;
                    }
                    cube.transform.parent = m_grid.nodes[target.m_indexNodeI, (target.m_indexNodeJ + i + 1)].transform;
                    cube.transform.DOMove(target.ObjStartPosition - new Vector3(0, 0, (i + 1)), Constant.DURATIONTIME).SetId(cube).OnComplete(() =>
                    {
                        //cube.transform.localPosition = new Vector3(0, 0, 0);
                    });
                    cube.ObjStartPosition = target.ObjStartPosition - new Vector3(0, 0, (i + 1));
                    cube.m_indexNodeI = target.m_indexNodeI;
                    cube.m_indexNodeJ = target.m_indexNodeJ + i + 1;
                }
            }
        }
        if (check == null)
        {
            InstantiateCube();
            CheckDeathZone();
            IsSetScore = false;
            yield break;
        }       
        var taskResult = m_level.LevelData.taskLevels;
        int count = 0;
        foreach (var item in taskResult)
        {
            Debug.Log("CHECK Complete Task");
            if (item.Amount == 0)
            {
                count++;
            }
            if(count == taskResult.Count)
            {
                Debug.Log("Complete Task");
                UIManager.Instance.UIEndGame.OnTriggerEventEndGame(StageEndGame.WIN);
                //GameManager.Instance.LoadNextLevelIndex(GameManager.Instance.CurrentIndexLevel);
                Debug.Log("===========END==========");
            }
        }
    }
    public void UpMultipleBall()
    {       
        ld = new List<GameObject>();
        foreach (Transform item in target.cubeContainer)
        {
            ld.Add(item.gameObject);
        }      
        for (int i = 0; i < ld.Count; i++)
        {
            CubeController cube = ld[i].transform.GetComponent<CubeController>();
            cube.transform.DOMove(target.ObjStartPosition - new Vector3(0, 0, (i + 1)), Constant.DURATIONTIME).SetId(cube);
            //cube.transform.parent = m_grid.nodes[target.m_indexNodeI, (target.m_indexNodeJ + i + 1)].transform;
            if (target.IsOnGrid == false)
            {
                cube.transform.parent = m_grid.nodes[target.m_indexNodeI, (target.m_indexNodeJ + i + 1)].transform;
                cube.m_indexNodeI = target.m_indexNodeI;
                cube.m_indexNodeJ = (target.m_indexNodeJ + i + 1);
            }
            cube.GetComponent<CubeController>().ObjStartPosition = target.ObjStartPosition - new Vector3(0, 0, (i + 1));
            if (target.m_indexNodeJ == 0 || target.transform.childCount == 0)
            {
                cube.transform.parent = m_grid.nodes[target.m_indexNodeI, (target.m_indexNodeJ + i + 1)].transform;
                cube.m_indexNodeI = target.m_indexNodeI;
                cube.m_indexNodeJ = (target.m_indexNodeJ + i + 1);
            }
        }       
    }

    public IEnumerator CalculatorOnTop(int indexI, int indexJ)
    {
        Debug.Log(" - m_grid.height " + (-m_grid.height));   
        Debug.Log("m_countDestroyCubeSpecial " + m_countDestroyCubeSpecial);       
        if (indexJ - 1 < 0|| m_grid.GetNode(indexI, indexJ).transform.childCount == 0)
        {
            Debug.Log(m_grid.GetNode(indexI, indexJ).transform.childCount == 0);
            Debug.Log("=======================> Calculator");
            Debug.Log("m_player.IsSetScore " + (IsSetScore));
            if (IsSetScore == false)
            {
                InstantiateCube();
                CheckDeathZone();
                IsSetScore = false;
                
            }
            yield break;
        }
        Debug.Log("CalculatorOnTop");
        CubeController cubeTarget = m_grid.GetNode(indexI, indexJ).transform.GetChild(0).GetComponent<CubeController>();
        CubeController cubeObj = m_grid.GetNode(indexI, indexJ - 1).transform.GetChild(0).GetComponent<CubeController>();
        BallScriptableObject check = GameApp.Instance.MixColors(cubeTarget.m_setUpBall.Ball.ColorType, cubeObj.m_setUpBall.Ball.ColorType);
        Debug.Log("check " + check);
        Debug.Log(cubeTarget.m_setUpBall.m_value);
        Debug.Log(cubeObj.m_setUpBall.m_value);
        if (check!=null)
        {
            Debug.Log("chay ");
            IsSetScore = true;
            //cubeTarget.m_setUpBall.m_value += cubeObj.m_setUpBall.m_value;                       
            //sum = cubeTarget.m_setUpBall.m_value;
            if(cubeTarget.m_setUpBall.Ball.ColorType == ColorType.BLACK)
            {
                Debug.Log("ColorType.BLACK");
            }
            Debug.Log("cubeTarget.m_setUpBall.Ball.ColorType " + cubeTarget.m_setUpBall.Ball.ColorType);
            Debug.Log("cubeObj.m_setUpBall.Ball.ColorType " + cubeObj.m_setUpBall.Ball.ColorType);
            BallScriptableObject m_colorType = GameApp.Instance.MixColors(cubeTarget.m_setUpBall.Ball.ColorType, cubeObj.m_setUpBall.Ball.ColorType);
            Debug.Log("GameApp.Instance.BallScriptables[m_colorType.ColorType] " + m_colorType.ColorType);
            cubeTarget.m_setUpBall.UpdateValue(GameApp.Instance.BallScriptables[m_colorType.ColorType]);
            cubeTarget.SetColor();
            cubeTarget.ObjStartPosition = cubeObj.ObjStartPosition;
            cubeTarget.transform.parent = cubeObj.transform.parent;
            cubeTarget.transform.DOMove(cubeObj.ObjStartPosition, Constant.DURATIONTIME).SetId(this).OnComplete(() =>
            {
                //cubeTarget.transform.localPosition = new Vector3(0,0,0);
            });
            yield return new WaitForSeconds(Constant.AFTERMOVETIME);
            cubeTarget.m_indexNodeI = cubeObj.m_indexNodeI;
            cubeTarget.m_indexNodeJ = cubeObj.m_indexNodeJ;
            int index = cubes.FindIndex(item => item == cubeObj);
            if (index >= 0)
            {
                cubes.RemoveAt(index);
            }
            DestroyImmediate(cubeObj.gameObject);
            Vector3 newPos = new Vector3(0, 0, Constant.BALLDESTROYPOSZ);
            var task = m_level.LevelData.taskLevels;
            foreach (var item in task)
            {
                if (cubeTarget.m_setUpBall.Ball.ColorType == item.ColorType && item.Amount >= 0)
                {
                    item.Amount--;
                    m_level.LevelUpdateTask();
                    cubeTarget.transform.DOMove(newPos, Constant.DURATIONTIME).SetId(cubeTarget);
                    cubeTarget.transform.parent = null;
                    cubeTarget.IsDestroy = true;
                    Destroy(cubeTarget.gameObject, Constant.DESTROYBALLTIME);                   
                    Debug.Log("cubeTargetDestroy " + " ======== " + (cubeTarget.IsDestroy == true));
                    m_countDestroyCubeSpecial++;
                }
            }
            //if (cubeTarget.m_setUpBall.Ball.ColorType == ColorType.YELLOW)
            //{               
            //    cubeTarget.transform.DOMove(newPos, Constant.DurationTime).SetId(cubeTarget);
            //    cubeTarget.transform.parent = null;
            //    cubeTarget.IsDestroy = true;
            //    Destroy(cubeTarget.gameObject, Constant.DestroyBallTime);
            //    Debug.Log("cubeTargetDestroy " + " ======== " + (cubeTarget.IsDestroy == true));
            //    m_countDestroyCubeSpecial++;
            //}
        }
        if (cubeTarget.IsDestroy == true)
        {
            Debug.Log("cubeTarget == null ");
            Debug.Log("cubeTargetDestroy " + cubeTarget.name);
            for (int i = 0; i < ld.Count; i++)
            {
                Debug.Log("chay UpMultipleBall Destroy");
                Debug.Log($"ld{i} " + ld[i].name);
                Debug.Log(target.m_indexNodeI + " " + (target.m_indexNodeJ + i) + " " + i);
                CubeController cube = ld[i].transform.GetComponent<CubeController>();
                if (target != null)
                {
                    Debug.Log("target != null " + target != null);
                    Debug.Log(target.ObjStartPosition - new Vector3(0, 0, (i)));
                    Debug.Log(cube.GetComponent<CubeController>().m_setUpBall.m_value);
                    //cubeTarget = ld[0].transform.GetComponent<CubeController>();
                    cube.transform.parent = m_grid.nodes[target.m_indexNodeI, (target.m_indexNodeJ + i)].transform;
                    cube.transform.DOMove(target.ObjStartPosition - new Vector3(0, 0, (i)), Constant.DURATIONTIME).SetId(cube).OnComplete(() =>
                    {
                        //cube.transform.localPosition = new Vector3(0, 0, 0);
                    });
                    cube.ObjStartPosition = target.ObjStartPosition - new Vector3(0, 0, (i));
                    cube.m_indexNodeI = target.m_indexNodeI;
                    cube.m_indexNodeJ = target.m_indexNodeJ + i;
                }
            }
        }
        if (indexJ - 1 == 0 || check == null)
        {
            Debug.Log("!check");
            if (m_countDestroyCubeSpecial != 0)
            {
                Debug.Log("m_countDestroyCubeSpecial != 0");
                //CubeController cube0 = ld[0].transform.GetComponent<CubeController>();
                Debug.Log("cube0");
                for (int i = 0; i < ld.Count; i++)
                {
                    CubeController cube0 = ld[0].transform.GetComponent<CubeController>();
                    Debug.Log("chay UpMultipleBall target destroy");
                    CubeController cube = ld[i].transform.GetComponent<CubeController>();
                    Debug.Log($"ld{i} " + ld[i].name + cube.m_setUpBall.m_value);
                    Debug.Log(cubeTarget.m_setUpBall.m_value);
                    Debug.Log($"ld{i} " + ld[i].transform.GetComponent<CubeController>().m_setUpBall.m_value);
                    Debug.Log(cube0.m_setUpBall.m_value);
                    Debug.Log("ld[i].transform == cubeTarget " + (cube.m_setUpBall.m_value == cubeTarget.m_setUpBall.m_value));
                    if (target != null)
                    {
                        cube.transform.parent = m_grid.nodes[cube0.m_indexNodeI, (cube0.m_indexNodeJ + i)].transform;
                        cube.transform.DOMove(cube0.ObjStartPosition - new Vector3(0, 0, (i)), Constant.DURATIONTIME).SetId(cube).OnComplete(() =>
                        {
                            //cube.transform.localPosition = new Vector3(0, 0, 0);
                        });
                        cube.ObjStartPosition = cube0.ObjStartPosition - new Vector3(0, 0, (i));
                        cube.m_indexNodeI = cube0.m_indexNodeI;
                        cube.m_indexNodeJ = cube0.m_indexNodeJ + i;
                    }
                }
            }
            if (m_countDestroyCubeSpecial == 0)
            {
                for (int i = 0; i < ld.Count; i++)
                {
                    Debug.Log("chay UpMultipleBall");
                    Debug.Log("ld[0] " + (ld[0].transform.GetComponent<CubeController>().m_setUpBall.m_value == cubeTarget.m_setUpBall.m_value));
                    CubeController cube = ld[i].transform.GetComponent<CubeController>();
                    Debug.Log(target.m_indexNodeI + " target.m_indexNode " + target.m_indexNodeJ);
                    if (target != null)
                    {
                        Debug.Log("cube i " + i);
                        cube.transform.parent = m_grid.nodes[target.m_indexNodeI, (target.m_indexNodeJ + i + 1)].transform;
                        cube.transform.DOMove(target.ObjStartPosition - new Vector3(0, 0, (i + 1)), Constant.DURATIONTIME).SetId(cube).OnComplete(() =>
                        {
                            //cube.transform.localPosition = new Vector3(0, 0, 0);
                        });
                        cube.ObjStartPosition = target.ObjStartPosition - new Vector3(0, 0, (i + 1));
                        cube.m_indexNodeI = target.m_indexNodeI;
                        cube.m_indexNodeJ = target.m_indexNodeJ + i + 1;
                    }
                }
            }            
            if (m_grid.nodes[cubeTarget.m_indexNodeI, cubeTarget.m_indexNodeJ + 1].IsChild())
            {
                CubeController cube = m_grid.nodes[cubeTarget.m_indexNodeI, cubeTarget.m_indexNodeJ + 1].transform.GetChild(0).GetComponent<CubeController>();
                if (cube.m_setUpBall.m_value == cubeTarget.m_setUpBall.m_value)
                {
                    yield return new WaitForSeconds(Constant.DURATIONTIME);
                    StartCoroutine(CalculatorOnBottom(cubeTarget.m_indexNodeI, cubeTarget.m_indexNodeJ));
                }
                //CalculatorOnBottom(cubeTarget.m_indexNodeI, cubeTarget.m_indexNodeJ);
            }
        }

        yield return new WaitForSeconds(Constant.DURATIONTIME);
        StartCoroutine(CalculatorOnTop(indexI, (indexJ - 1)));
    }

    public IEnumerator CalculatorOnBottom(int indexI, int indexJ)
    {
        if (indexJ + 1 >= (m_grid.height - 1) || m_grid.GetNode(indexI, indexJ + 1).IsChild() == false)
        {
            Debug.Log("=======================> CalculatorOnBottom");
            yield break;
        }
        Debug.Log("CalculatorOnBottom");
        Debug.Log("m_countDestroyCubeSpecial " + m_countDestroyCubeSpecial);
        CubeController cubeTargetBot = m_grid.GetNode(indexI, indexJ).transform.GetChild(0).GetComponent<CubeController>();
        CubeController cubeObjBot = m_grid.GetNode(indexI, indexJ + 1).transform.GetChild(0).GetComponent<CubeController>();
        BallScriptableObject check = GameApp.Instance.MixColors(cubeObjBot.m_setUpBall.Ball.ColorType, cubeTargetBot.m_setUpBall.Ball.ColorType);
        if (check == null)
        {
            yield break;
        }
        if (check!=null)
        {
            //cubeObjBot.m_setUpBall.m_value += cubeTargetBot.m_setUpBall.m_value;
            //sum = cubeObjBot.m_setUpBall.m_value;
            BallScriptableObject m_colorType = GameApp.Instance.MixColors(cubeObjBot.m_setUpBall.Ball.ColorType, cubeTargetBot.m_setUpBall.Ball.ColorType);            
            cubeObjBot.m_setUpBall.UpdateValue(GameApp.Instance.BallScriptables[m_colorType.ColorType]);           
            cubeObjBot.ObjStartPosition = cubeTargetBot.ObjStartPosition;
            if (cubeTargetBot != null)
            {
                cubeObjBot.transform.parent = m_grid.nodes[cubeTargetBot.m_indexNodeI, cubeTargetBot.m_indexNodeJ].transform;
                cubeObjBot.transform.DOMove(cubeTargetBot.ObjStartPosition, Constant.DURATIONTIME).SetId(this).OnComplete(() =>
                {
                    cubeObjBot.transform.localPosition = new Vector3(0, 0, 0);
                });
                cubeObjBot.SetColor();
                cubeObjBot.ObjStartPosition = cubeTargetBot.ObjStartPosition;
                cubeObjBot.m_indexNodeI = cubeTargetBot.m_indexNodeI;
                cubeObjBot.m_indexNodeJ = cubeTargetBot.m_indexNodeJ;
                int index = cubes.FindIndex(item => item == cubeTargetBot);
                if (index >= 0)
                {
                    cubes.RemoveAt(index);
                }
                cubeTargetBot.gameObject.SetActive(false);
                Destroy(cubeTargetBot.gameObject, Constant.DURATIONTIME);
            }
            if (m_countDestroyCubeSpecial != 0)
            {
                for (int i = 0; i < ld.Count; i++)
                {
                    if (i == 0)
                    {
                        continue;
                    }
                    Debug.Log("m_countDestroyCubeSpecial != 0 ====== CalculatorOnBottom");
                    CubeController cube = ld[i].transform.GetComponent<CubeController>();
                    Debug.Log("CalculatorOnBottom " + $"Ld{i}.name " + cube.m_setUpBall.m_value + "    " + i);
                    Debug.Log("CubeObjBot " + cubeObjBot.ObjStartPosition + "==== " + cubeObjBot.m_setUpBall.m_value);
                    Debug.Log(cubeObjBot.ObjStartPosition - new Vector3(0, 0, i - m_countDestroyCubeSpecial ));
                    cube.transform.DOMove(cubeObjBot.ObjStartPosition - new Vector3(0, 0, i - 1), Constant.DURATIONTIME).SetId(cube);
                    Debug.Log("cube.transform " + cube.transform.position);
                    cube.transform.parent = m_grid.nodes[cubeObjBot.m_indexNodeI, (cubeObjBot.m_indexNodeJ + i - m_countDestroyCubeSpecial)].transform;
                    cube.m_indexNodeI = cubeObjBot.m_indexNodeI;
                    cube.m_indexNodeJ = cubeObjBot.m_indexNodeJ + i - m_countDestroyCubeSpecial;
                }
            }
            if(m_countDestroyCubeSpecial == 0)
            {
                for (int i = 0; i < ld.Count; i++)
                {
                    CubeController cube = ld[i].transform.GetComponent<CubeController>();
                    Debug.Log("CalculatorOnBottom " + $"Ld{i}.name " + cube.m_setUpBall.m_value + "    " + i);
                    Debug.Log(cubeObjBot.ObjStartPosition - new Vector3(0, 0, i));
                    cube.transform.DOMove(cubeObjBot.ObjStartPosition - new Vector3(0, 0, i), Constant.DURATIONTIME).SetId(cube);
                    Debug.Log("cube.transform " + cube.transform.position);
                    cube.transform.parent = m_grid.nodes[cubeObjBot.m_indexNodeI, (cubeObjBot.m_indexNodeJ + i)].transform;
                    cube.m_indexNodeI = cubeObjBot.m_indexNodeI;
                    cube.m_indexNodeJ = cubeObjBot.m_indexNodeJ + i;
                }
            }            
        }        
        Debug.Log("==============================================");
        //return CalculatorOnBottom(indexI, (indexJ + 1));
        yield return new WaitForSeconds(Constant.DURATIONTIME);
        StartCoroutine(CalculatorOnBottom(indexI, (indexJ + 1)));
    }   
    public void InstantiateCube()
    {
        
        if (m_cubeSpawn != null)
        {            
            foreach (CubeController icube in cubes)
            {
                icube.transform.DOMove(icube.ObjStartPosition - new Vector3(0, 0, m_level.LevelData.spacePer), Constant.DURATIONTIME).SetId(icube).OnComplete(() =>
                {
                    if ((icube.m_indexNodeJ + 1) >= m_grid.height)
                    {
                        UIManager.Instance.UIEndGame.OnTriggerEventEndGame(StageEndGame.LOSE);
                        Debug.Log("Run Return");
                    return;
                    }
                    icube.transform.parent = m_grid.nodes[icube.m_indexNodeI, (icube.m_indexNodeJ + 1)].transform;
                    icube.m_indexNodeJ += 1;
                    icube.ObjStartPosition -= new Vector3(0, 0, m_level.LevelData.spacePer);
                });                            
            }
            float space =0;
            for (int ik = 0; ik < m_grid.width; ik++)
            {               
                Vector3 spawnPos = new Vector3(space, 0, 2f);
                Vector3 spawnMove = new Vector3(space, 0, 0);
                space += m_level.LevelData.spacePer;
                GameObject cube = Instantiate(m_cubeSpawn.gameObject, spawnPos, Quaternion.identity);
                CubeController m_spawn = cube.transform.GetComponent<CubeController>();
                m_spawn.m_player = this;
                m_spawn.transform.parent = m_grid.nodes[ik, 0].transform;
                m_spawn.transform.DOMove(spawnMove, Constant.DURATIONTIME).SetId(this).OnComplete(() => {
                    m_spawn.ObjStartPosition = spawnMove;
                    Debug.Log("I ==== " + ik);
                    m_spawn.m_placeObjectOnGrid = m_grid;
                    m_spawn.m_player = this;                                                    
                });
                cubes.Add(m_spawn);
                m_spawn.m_indexNodeI = ik;
                m_spawn.m_indexNodeJ = 0;
                m_spawn.m_setUpBall.UpdateValue(GameApp.Instance.GetColorRandom());
                m_spawn.m_renderer = m_spawn.gameObject.GetComponent<Renderer>();
                m_spawn.SetColor();                
            }                 
        }              
    }
    public void CheckDeathZone()
    {
        foreach (CubeController item in cubes)
        {
            if (item.ObjStartPosition.z <= -m_grid.height)
            {
                UIManager.Instance.UIEndGame.OnTriggerEventEndGame(StageEndGame.LOSE);
                Debug.Log("=============================================");
            }
        }
    }
}