using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeController : MonoBehaviour
{
    public void SetUpNodeController(bool isPlaceable, Vector3 cellPosition, NodeController obj, CubeController cube)
    {
    }

    public bool IsChild()
    {
        return transform.childCount > 0;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.2f);
    }
}