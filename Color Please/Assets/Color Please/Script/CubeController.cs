using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CubeController : MonoBehaviour
{
    public Transform cube;
    public bool IsOnGrid;
    public bool IsDestroy = false;
    public Vector3 ObjStartPosition;
    public Vector3 SmoothMousePosition;
    public Renderer m_renderer;
    public Vector3 m_mousePosition;
    public SetUpBall m_setUpBall;

    public int m_indexNodeI;
    public int m_indexNodeJ;
    public PlayerController m_player;
    public GridController m_placeObjectOnGrid;

    public Transform cubeContainer;
    private float m_count;

    public void Setup(GridController placeObjectOnGrid, PlayerController player, int x, int y)
    {
        m_player = player;
        m_placeObjectOnGrid = placeObjectOnGrid;
        m_renderer = gameObject.GetComponent<Renderer>();
        int codeColor = m_player.Level.LevelData.colorPoints.Find(item => item.indexX == x && item.indexY == y).colorCode;
        ColorType colorType = GameManager.Instance.GetTT(codeColor);
        BallScriptableObject ballScriptable = GameApp.Instance.BallScriptables[colorType];
        m_setUpBall.UpdateValue(ballScriptable);
        SetColor();
        IsOnGrid = true;
        m_indexNodeI = x;
        m_indexNodeJ = y;
        ObjStartPosition = transform.position;
    }

    private void Update()
    {
        if(m_player != null && m_player.Level != null && m_player.Level.StageLevel != StageLevel.Playing)
        {
            return;
        }
        GetMousePositionOnGrid();
    }

    private void GetMousePositionOnGrid()
    {
        if (m_player == null || (this != m_player.GetTarget && m_player.GetTarget != null))
        {
            return;
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (m_placeObjectOnGrid.Plane.Raycast(ray, out var enter))
        {
            m_mousePosition = ray.GetPoint(enter);
            SmoothMousePosition = m_mousePosition;
            m_mousePosition.y = 0;
            m_mousePosition = Vector3Int.RoundToInt(m_mousePosition);
        }
    }

    private void CheckIsOnGrid()
    {
        for (int i = 0; i < m_placeObjectOnGrid.width; i++)
        {
            for (int j = 0; j < m_placeObjectOnGrid.height; j++)
            {
                if (this.transform.parent == m_placeObjectOnGrid.nodes[i, j].transform)
                {
                    this.IsOnGrid = true;
                }
            }
        }
    }

    private void OnMouseDown()
    {
        if (m_player.Level.StageLevel != StageLevel.Playing)
        {
            return;
        }
        m_player.m_countDestroyCubeSpecial = 0;
        m_player.IsSetScore = false;
        if (this.IsOnGrid == true)
        {
            this.IsOnGrid = false;
            this.ObjStartPosition = this.transform.position;
        }
       
    }

    private void OnMouseDrag()
    {
        if (m_player.Level.StageLevel != StageLevel.Playing)
        {
            return;
        }
        m_placeObjectOnGrid.nodes[m_indexNodeI, m_indexNodeJ].transform.DetachChildren();
        m_player.SetTarget(this);
        m_player.DragMultipleBall();
        if (!this.IsOnGrid)
        {
            this.transform.position = SmoothMousePosition
             + new Vector3(0, Constant.YPOSITIONONGRID, 0);
        }
    }

    private void OnMouseUp()
    {
        if ((this != m_player.GetTarget && m_player.GetTarget != null) || m_player.Level.StageLevel != StageLevel.Playing)
        {
            return;
        }
        //Debug.Log("m_player.BG.SetActive(true) " + );
        Debug.Log("Target: " + m_player.GetTarget);
        for (int i = 0; i < m_placeObjectOnGrid.width; i++)
        {
            for (int j = 0; j < m_placeObjectOnGrid.height; j++)
            {
                if (m_placeObjectOnGrid.nodes[i, j].transform.position.x == m_mousePosition.x && m_placeObjectOnGrid.nodes[i, j].IsChild() == false)
                {
                    if (this.IsOnGrid == false && m_placeObjectOnGrid.nodes[i, j].transform.position.x != this.ObjStartPosition.x)
                    {
                        m_player.CheckOutSide();                       
                        this.IsOnGrid = true;
                        m_indexNodeI = i;
                        m_indexNodeJ = j;
                        this.transform.parent = m_placeObjectOnGrid.nodes[i, j].transform;
                        Vector3 newPos = m_placeObjectOnGrid.nodes[i, j].transform.position;
                        this.transform.DOMove(newPos, Constant.DURATIONTIME).SetId(this).OnComplete(() =>
                        {
                            //this.transform.localPosition = new Vector3(0, 0, 0); 
                        });
                        //this.transform.parent = m_placeObjectOnGrid.nodes[i, j].transform;
                        //this.transform.localPosition = new Vector3(0, 0, 0);
                        this.ObjStartPosition = newPos;
                        if (this.transform.childCount >= 2)
                        {
                            m_player.UpMultipleBall();                            
                        }
                        //StartCoroutine(m_player.CalculatorOnTop(i, j));
                        StartCoroutine(m_player.DropMultipleCube(i, j));
                        //m_player.m_countDestroyCubeSpecial = 0;                        
                        SetColor();
                        return;
                    }
                }
                if (m_placeObjectOnGrid.nodes[i, j].transform.position == m_mousePosition && m_placeObjectOnGrid.nodes[i, j].IsChild() && m_placeObjectOnGrid.nodes[i, j].transform.childCount == 1)
                {
                    this.IsOnGrid = false;
                }
            }
        }
        if (this.IsOnGrid == false)
        {
            this.transform.parent = m_placeObjectOnGrid.nodes[m_indexNodeI, m_indexNodeJ].transform;

            transform.DOMove(this.ObjStartPosition, Constant.DURATIONTIME).SetId(this);

            m_player.UpMultipleBall();
        }
        this.IsOnGrid = true;
    }

    private void SetColorRed()
    {
        m_renderer.material.color = Color.red;
    }

    public void SetColor()
    {
        if (m_renderer == null)
        {
            m_renderer = gameObject.GetComponent<Renderer>();
        }
        m_renderer.material.color = m_setUpBall.Color;
    }

    public void Init(PlayerController playerController)
    {
        m_player = playerController;
        cube.gameObject.SetActive(true);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    private void OnDestroy()
    {
        this.IsDestroy = true;
        DOTween.Kill(this);
    }
}