using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameApp : SingletonMono<GameApp>
{
    [SerializeField] private BallScriptableObject[] ballScriptables;
    [SerializeField] private List<BallScriptableObject> colorsPrimary;
    [SerializeField] private List<BallScriptableObject> colorsIntermediary;
    public Dictionary<ColorType, BallScriptableObject> BallScriptables;
    public LevelContainer LevelContainer;

    private void Awake()
    {
        // Load Resource
        // Load into Dictionary
        OnLoadColorConfigs();
        OnLoadLevelConfig();
    }

    private void OnLoadColorConfigs()
    {
        BallScriptables = new Dictionary<ColorType, BallScriptableObject>();
        foreach (var item in ballScriptables)
        {
            if (!BallScriptables.ContainsKey(item.ColorType))
            {
                BallScriptables.Add(item.ColorType, item);
                if (item.ColorGroup == ColorGroup.PRIMARY)
                {
                    colorsPrimary.Add(item);
                }
                else if (item.ColorGroup == ColorGroup.INTERMEDIARY)
                {
                    colorsIntermediary.Add(item);
                }
            }
        }
    }
    

    private void OnLoadLevelConfig()
    {
        var read = Resources.Load<TextAsset>("Levels/levels");
        JsonUtility.FromJsonOverwrite(read.ToString(), LevelContainer);
    }

    public BallScriptableObject MixColors(ColorType color_1, ColorType color_2)
    {
        foreach (var item in colorsIntermediary)
        {
            if (CheckMixColor(item, color_1, color_2))
            {
                return item;
            }
        }
        return null;
    }

    private bool CheckMixColor(BallScriptableObject item, ColorType color_1, ColorType color_2)
    {
        if (color_1 == color_2 || item.ColorsRequestMix.Count < 1) return false;
        return (color_1 == item.ColorsRequestMix[0] || color_1 == item.ColorsRequestMix[1]) && (color_2 == item.ColorsRequestMix[0] || color_2 == item.ColorsRequestMix[1]);
    }
    public BallScriptableObject GetColorRandom()
    {
        return colorsPrimary[UnityEngine.Random.Range(0, colorsPrimary.Count)];
    }
}