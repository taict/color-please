using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : SingletonMonoAwake<UIManager>
{
    public UIMenuController UIMenu;
    public UIGameplayController UIGameplay;
    public UIEndGameController UIEndGame;
    public UIPopupMessageController UIPopupMessage;

    public override void OnAwake()
    {
        base.OnAwake();
        InitUI();
    }

    private void Start()
    {
        UIMenu.Show();
    }

    private void InitUI()
    {
        foreach (Transform item in transform)
        {
            UIBase uIBase = item.GetComponent<UIBase>();
            uIBase.SetupUI();
            uIBase.Hide();
        }
    }
}