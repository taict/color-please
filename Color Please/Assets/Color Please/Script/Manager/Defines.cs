using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum ColorType
{
    NONE = 0,
    RED = 2,
    GREEN = 4,
    BLUE = 8,
    YELLOW = 16,
    ORANGE = 32,
    PINK = 64,
    PURPLE = 128,
    AQUA = 256,
    BLUEQUA = 512,
    MAGENTA = 1024,
    CYAN = 2048,
    BLACK,
    WHITE,
    TEAL,
    PEACH
}

public enum ColorGroup
{
    PRIMARY,
    INTERMEDIARY
}

public enum StageLevel
{
    Pre,
    Playing,
    End
}

public enum StageEndGame
{
    WIN,
    LOSE
}

public static class Constant
{
    public static readonly float DURATIONTIME = 0.3f;
    public static readonly float YPOSITIONONGRID = 0.2f;
    public static float AFTERMOVETIME = 0.2f;
    public static float DESTROYBALLTIME = 0.2f;
    public static readonly float BALLDESTROYPOSZ = -11f;
}

[Serializable]
public class TaskLevel
{
    public ColorType ColorType;
    public int Amount;
}