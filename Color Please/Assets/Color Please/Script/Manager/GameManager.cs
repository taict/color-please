using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonMonoAwake<GameManager>
{
    [SerializeField] private int m_CurrentIndexLevel;
    [SerializeField] private LevelController[] m_Levels;
    [SerializeField] private LevelController m_CurrentLevel;
    [SerializeField] private LevelController m_OldLevel;

    public int CurrentIndexLevel { get => m_CurrentIndexLevel; }

    public LevelController CurrentLevel { get => m_CurrentLevel; set => m_CurrentLevel = value; }

    public override void OnAwake()
    {
        base.OnAwake();
        UIMenuController.EventStartWithLevel += OnLoadLevel;
    }

    private void OnDestroy()
    {
        UIMenuController.EventStartWithLevel -= OnLoadLevel;
    }

    private void OnLoadLevel()
    {
        LoadLevelIndex(m_CurrentIndexLevel);
    }

    public void LoadLevelIndex(int indexLevel)
    {
        if (m_CurrentLevel != null)
        {
            DestroyImmediate(m_CurrentLevel.gameObject);
        }
        m_CurrentLevel = Instantiate(m_Levels[0], transform);
        m_CurrentLevel.gameObject.SetActive(true);
         var m_levelData = new LevelData()
        {
            taskLevels = new List<TaskLevel>(),
            colorPoints = new List<ColorPoint>(),
            levelID = GameApp.Instance.LevelContainer.levelDatas[indexLevel].levelID,
            sizeX = GameApp.Instance.LevelContainer.levelDatas[indexLevel].sizeX,
            sizeY = GameApp.Instance.LevelContainer.levelDatas[indexLevel].sizeY,
            spacePer = GameApp.Instance.LevelContainer.levelDatas[indexLevel].spacePer
        };
        m_levelData.taskLevels.AddRange(GameApp.Instance.LevelContainer.levelDatas[indexLevel].taskLevels.ToArray());
        m_levelData.colorPoints.AddRange(GameApp.Instance.LevelContainer.levelDatas[indexLevel].colorPoints.ToArray());
        m_CurrentLevel.InitLevel(m_levelData);
    }
    public void LoadNextLevelIndex(int indexLevel)
    {

        if (m_CurrentLevel != null)
        {
            DestroyImmediate(m_CurrentLevel.gameObject);
        }
        m_CurrentIndexLevel++;
        m_CurrentLevel = Instantiate(m_Levels[0], transform);
        m_CurrentLevel.gameObject.SetActive(true);

        m_CurrentLevel.InitLevel(GameApp.Instance.LevelContainer.levelDatas[indexLevel]);
    }
    public void LoadNextLevel()
    {
        if (m_CurrentLevel != null)
        {
            Destroy(m_CurrentLevel.gameObject);
        }
        if(m_CurrentIndexLevel > m_Levels.Length)
        {
            return;
        }
        m_CurrentIndexLevel++;
        m_CurrentLevel = Instantiate(m_Levels[0], transform);
        m_CurrentLevel.gameObject.SetActive(true);

        m_CurrentLevel.InitLevel(GameApp.Instance.LevelContainer.levelDatas[m_CurrentIndexLevel]);

    }
    public void LoadReplayLevel()
    {
        if (m_CurrentLevel != null)
        {
            Destroy(m_CurrentLevel.gameObject);
        }
        m_CurrentLevel = Instantiate(m_Levels[0], transform);
        m_CurrentLevel.gameObject.SetActive(true);

        m_CurrentLevel.InitLevel(GameApp.Instance.LevelContainer.levelDatas[m_CurrentIndexLevel]);
    }
    public ColorType GetTT(int number)
    {
        ColorType colorType = ColorType.RED;
        switch (number)
        {
            case 4:
                colorType = ColorType.GREEN;
                break;

            case 8:
                colorType = ColorType.BLUE;
                break;

            case 16:
                colorType = ColorType.YELLOW;
                break;

            case 32:
                colorType = ColorType.ORANGE;
                break;

            case 64:
                colorType = ColorType.PINK;
                break;

            case 128:
                colorType = ColorType.PURPLE;
                break;

            case 256:
                colorType = ColorType.AQUA;
                break;

            case 512:
                colorType = ColorType.BLUEQUA;
                break;

            case 1024:
                colorType = ColorType.MAGENTA;
                break;

            case 2048:
                colorType = ColorType.CYAN;
                break;
        }
        return colorType;
    }
}