﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UserProfileManager : SingletonMonoAwake<UserProfileManager>
{
    [SerializeField] private UserProfile m_UserProfile;
    private string TempFilePath;
    private bool isNewer = false;

    public UserProfile UserProfile { get => m_UserProfile; set => m_UserProfile = value; }
    public bool IsNewer { get => isNewer; set => isNewer = value; }

    public void Init(Action OnComplete)
    {
        TempFilePath = Application.persistentDataPath + "/UserProfile.data";
        Debug.LogError("TempFilePath ==> " + TempFilePath);

        if (!File.Exists(TempFilePath))//make a file if not exists
        {
            isNewer = true;
            using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
            {
                m_UserProfile = new UserProfile();
                newTask.WriteLine(JsonUtility.ToJson(m_UserProfile));
                newTask.Close();
            }
        }
        else// read and update file
        {
            StreamReader reader = new StreamReader(TempFilePath);
            JsonUtility.FromJsonOverwrite(reader.ReadLine(), m_UserProfile);
            reader.Close();
        }

        OnComplete?.Invoke();
    }

    private void SaveData()
    {
        if (TempFilePath != null)
        {
            using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
            {
                string str = JsonUtility.ToJson(m_UserProfile);
                newTask.WriteLine(str);
                newTask.Close();
            }
        }
    }

    private void OnApplicationPause(bool pause)
    {
        SaveData();
    }

    private void OnApplicationQuit()
    {
        SaveData();
    }
}

[System.Serializable]
public class UserProfile
{
    public int m_IndexLevelOld;
    public LevelInfo m_LevelOldInfo;

    public UserProfile()
    {
        m_IndexLevelOld = -1;
        m_LevelOldInfo = new LevelInfo();
    }
}

[System.Serializable]
public class LevelInfo
{
    public int m_IndexCurrentLayerDone;
    public List<int> m_LsIndexLayer;
    public List<string> m_LsLayerDone;
    public List<string> m_LsObjectDone;

    public LevelInfo()
    {
        m_IndexCurrentLayerDone = -1;
        m_LsIndexLayer = new List<int>();
        m_LsLayerDone = new List<string>();
        m_LsObjectDone = new List<string>();
    }
}